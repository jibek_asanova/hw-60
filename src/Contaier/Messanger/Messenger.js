import React, {useEffect, useState} from 'react';
import './Messenger.css';
import Message from "../../Component/Message/Message";
import Form from "../../Component/Form/Form";

const url = 'http://146.185.154.90:8000/messages';

const Messenger = () => {
    const [messages, setMessages] = useState([]);
    const [example, setExample] = useState({author: '', message: ''});
    const [dateTime, setDateTime] = useState(null);


    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if(response.ok) {
                const messages = await response.json();
                setMessages(messages);
                const lastMessage = messages[messages.length - 1];
                setDateTime(lastMessage.datetime);
            }
        };

        fetchData().catch(e => console.error(e));

    },[]);


    useEffect(() => {
        const interval = setInterval(() => {
            const fetchData = async () => {
                const response = await fetch(`http://146.185.154.90:8000/messages?datetime=${dateTime}`);
                if(response.ok) {
                    const newMessages = await response.json();
                    if(newMessages.length !== 0) {
                        setMessages(newMessages.reverse().concat(messages));
                    }
                }
            };

            fetchData().catch(e => console.error(e));
        }, 2000);
        return () => clearInterval(interval);

    }, [dateTime]);

    const onMessageChange = e => {
        const {name, value} = e.target
        setExample({...example, [name] : value});
    }

    const sendMessage = async (e) => {
        e.preventDefault();
        try {
            const data = new URLSearchParams();
            data.set('message', example.message);
            data.set('author', example.author);

           await fetch(url, {
                method: 'post',
                body: data,
            });
        } catch (e) {

        }
    }


    return (
        <div className="Messenger">
            <Form
                sendMessage={sendMessage}
                example={example}
                onMessageChange={onMessageChange}
            />
            <div className="MessageBlock">
                <Message
                    messages={messages}
                />
            </div>


        </div>
    )
};

export default Messenger;