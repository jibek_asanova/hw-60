import React from 'react';
import './Form.css';

const Form = ({example, onMessageChange, sendMessage}) => {
    return (
        <form className="messageForm">
            <div className="form-group">
                <label htmlFor="inputAuthor">Author:</label>
                <input
                    type="text"
                    onChange={onMessageChange}
                    name="author"
                    value={example.author}
                    className="form-control"
                    id="inputAuthor"
                />
            </div>
            <div className="form-group">
                <label htmlFor="inputMessage">Message:</label>
                <textarea
                    name="message"
                    onChange={onMessageChange}
                    value={example.message}
                    className="form-control"
                    id="inputMessage"
                    rows="3"
                />
            </div>
            <button type="submit" className="btn btn-primary" id="sendButton" onClick={sendMessage}>Send</button>
        </form>
    );
};

export default Form;