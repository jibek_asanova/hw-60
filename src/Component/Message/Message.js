import React from 'react';
import './Message.css';

const Message = (props) => {
    return (
        props.messages.map(message => (
                <div className="card"
                     key={message._id}
                >
                    <div className="card-header">
                        <strong>Author:</strong> {message.author}
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">Message:</h5>
                        <p className="card-text">{message.message}</p>
                        <p className="card-text"><strong>Time:</strong> {message.datetime}</p>
                    </div>
                </div>
            ))
    );
};

export default Message;